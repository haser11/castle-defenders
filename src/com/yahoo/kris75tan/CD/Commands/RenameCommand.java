package com.yahoo.kris75tan.CD.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;

public class RenameCommand implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(command.getName().equalsIgnoreCase("rename") && sender instanceof Player)
		{
			Player p = (Player) sender;
			if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("help"))
				{
					sender.sendMessage("/rename Renames the item in your hand");
					sender.sendMessage("Usage: /rename NAME");
					return true;
				}
				else 
				{	
					ItemMeta m = p.getItemInHand().getItemMeta();	
					m.setDisplayName(args[0]);
					p.getItemInHand().setItemMeta(m);
					return true;
				}
			}
			else if(args.length == 2)
			{
				ItemMeta m = p.getItemInHand().getItemMeta();	
				m.setDisplayName(args[0] + " " + args[1]);
				p.getItemInHand().setItemMeta(m);
				return true;
			}
			else
			{
				p.sendMessage("Name can't be longer than 2 words");
			}
		}
		return false;
	}

}
