package com.yahoo.kris75tan.CD.Commands;


import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.yahoo.kris75tan.CD.Items.CustomItems;

public class SnowballCommand implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(command.getName().equalsIgnoreCase("wall") && sender instanceof Player)
		{
			CustomItems ci = new CustomItems();
			Player p = (Player) sender;
			if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("help"))
				{
					sender.sendMessage("/wall gives the player a throwable wall item");
					sender.sendMessage("Usage: /wall WALLSIZE");
					sender.sendMessage("NOTE: WALLSIZE must be an odd number! eg: 3,5,7,9 etc");
					return true;
				}
				else
				{	
					p.getInventory().addItem(ci.snowball(args[0]));
					return true;
				}
			}
		}
		return false;
	}

}
