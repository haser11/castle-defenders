package com.yahoo.kris75tan.CD.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.yahoo.kris75tan.CD.Items.CustomItems;

public class TntCommand implements CommandExecutor
{
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(command.getName().equalsIgnoreCase("tnt") && sender instanceof Player)
		{
			CustomItems ci = new CustomItems();
			Player p = (Player) sender;
			if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("help"))
				{
					sender.sendMessage("/tnt gives the player the ThrowableTNT item");
					sender.sendMessage("Usage: /tnt AMOUNT");
					return true;
				}
				else
				{	
					int amount = Integer.parseInt(args[0]);
					ItemStack tntStack = new ItemStack(ci.tnt());
					tntStack.setAmount(amount);
					p.getInventory().addItem(tntStack);
					return true;
				}
			}
		}
		return false;
	}
}
