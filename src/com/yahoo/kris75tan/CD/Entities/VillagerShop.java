package com.yahoo.kris75tan.CD.Entities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.yahoo.kris75tan.CD.Items.CustomItems;

import net.md_5.bungee.api.ChatColor;

public class VillagerShop implements Listener
{	
	Inventory weapons = Bukkit.getServer().createInventory(null, 9, "Weapon Shop");
	Inventory magic = Bukkit.getServer().createInventory(null, 9, "Magic Shop");
	Inventory blacksmith = Bukkit.getServer().createInventory(null, 36, "Blacksmith");
	Inventory butcher = Bukkit.getServer().createInventory(null, 9, "Butcher");
	
	@EventHandler
	public void onInteract(PlayerInteractEntityEvent e)
	{
		Player p = e.getPlayer();
		Entity entity = e.getRightClicked();
		if(entity instanceof Villager)
		{
			if(getEntityName(entity) != null)
			{
				CustomItems ci = new CustomItems();
				if(getEntityName(entity).equalsIgnoreCase("Weapon Shop"))
				{
					//Clears inventory
					weapons.clear();
					
					//Populate inventory
					weapons.addItem(ci.snowball("3"));
					weapons.addItem(ci.snowball("5"));
					weapons.addItem(ci.snowball("7"));
					weapons.addItem(ci.tnt());
					weapons.addItem(ci.woodSword());
					weapons.addItem(ci.stoneSword());
					weapons.addItem(ci.ironSword());
					weapons.addItem(ci.diamondSword());
					
					//Cancel event and open inventory
					e.setCancelled(true);
					p.openInventory(weapons);
				}
				else if(getEntityName(entity).equalsIgnoreCase("Magic Shop"))
				{
					//Clears inventory
					magic.clear();
					
					//Populate inventory
					magic.addItem(ci.blazeRod(0));
					
					//Cancel event and open inventory
					e.setCancelled(true);
					p.openInventory(magic);
				}
				else if(getEntityName(entity).equalsIgnoreCase("Blacksmith"))
				{
					//Clears inventory
					blacksmith.clear();
					
					//Populate inventory
					
					//Leather Armor
					// 0 = Helm, 1 = Chest, 2 = Leg, 3 = Boot
					blacksmith.setItem(0, ci.leatherArmor(0));
					blacksmith.setItem(9, ci.leatherArmor(1));
					blacksmith.setItem(18, ci.leatherArmor(2));
					blacksmith.setItem(27, ci.leatherArmor(3));
					
					//Gold Armor
					blacksmith.setItem(1, ci.goldArmor(0));
					blacksmith.setItem(10, ci.goldArmor(1));
					blacksmith.setItem(19, ci.goldArmor(2));
					blacksmith.setItem(28, ci.goldArmor(3));
					
					//Iron Armor
					blacksmith.setItem(2, ci.ironArmor(0));
					blacksmith.setItem(11, ci.ironArmor(1));
					blacksmith.setItem(20, ci.ironArmor(2));
					blacksmith.setItem(29, ci.ironArmor(3));
					
					//Diamond Armor
					blacksmith.setItem(3, ci.diamondArmor(0));
					blacksmith.setItem(12, ci.diamondArmor(1));
					blacksmith.setItem(21, ci.diamondArmor(2));
					blacksmith.setItem(30, ci.diamondArmor(3));
					
					//Cancel event and open inventory
					e.setCancelled(true);
					p.openInventory(blacksmith);
				}
				else if(getEntityName(entity).equalsIgnoreCase("Butcher"))
				{
					//Clears inventory
					butcher.clear();
					
					//Populate inventory
					butcher.addItem(ci.beef());
					butcher.addItem(ci.chicken());
					butcher.addItem(ci.pork());
					butcher.addItem(ci.mutton());
					butcher.addItem(ci.stew());
					
					//Cancel event and open inventory
					e.setCancelled(true);
					p.openInventory(butcher);
				}
			}
		}
	}
	
	private String getEntityName(Entity entity)
	{
		if(entity.getCustomName() != null)
		{
			String name = entity.getCustomName();
			return name;
		}
		else
		{
			return null;
		}
	}
	
	@EventHandler
	public void onClickInventory(InventoryClickEvent e)
	{
		if(e.getClickedInventory().equals(weapons))
		{
			ItemStack clicked = e.getInventory().getItem(e.getSlot());
			CustomItems ci = new CustomItems();
			Player p = (Player) e.getWhoClicked();
			
			if(clicked.equals(ci.snowball("3")))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(ci.snowball("3"));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			if(clicked.equals(ci.snowball("5")))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(ci.snowball("5"));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			if(clicked.equals(ci.snowball("7")))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 7))
				{
					p.getInventory().addItem(ci.snowball("7"));
					removeCoins(p, 7);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			if(clicked.equals(ci.tnt()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 4))
				{
					p.getInventory().addItem(ci.tnt());
					removeCoins(p, 4);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			//Swords
			if(clicked.equals(ci.woodSword()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			if(clicked.equals(ci.stoneSword()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			if(clicked.equals(ci.ironSword()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			if(clicked.equals(ci.diamondSword()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 7))
				{
					p.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
					removeCoins(p, 7);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
		}
		else if(e.getClickedInventory().equals(butcher))
		{
			ItemStack clicked = e.getInventory().getItem(e.getSlot());
			CustomItems ci = new CustomItems();
			Player p = (Player) e.getWhoClicked();
			
			if(clicked.equals(ci.beef()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.pork()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.GRILLED_PORK));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.chicken()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.COOKED_CHICKEN));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.mutton()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.COOKED_MUTTON));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.stew()))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
				{
					p.getInventory().addItem(new ItemStack(Material.RABBIT_STEW));
					removeCoins(p, 2);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		}
		else if(e.getClickedInventory().equals(blacksmith))
		{
			ItemStack clicked = e.getInventory().getItem(e.getSlot());
			CustomItems ci = new CustomItems();
			Player p = (Player) e.getWhoClicked();
			
			//Leather Armor
			if(clicked.equals(ci.leatherArmor(0)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.LEATHER_HELMET));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.leatherArmor(1)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.LEATHER_CHESTPLATE));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.leatherArmor(2)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.LEATHER_LEGGINGS));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.leatherArmor(3)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 1))
				{
					p.getInventory().addItem(new ItemStack(Material.LEATHER_BOOTS));
					removeCoins(p, 1);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			//Gold Armor
			else if(clicked.equals(ci.goldArmor(0)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
				{
					p.getInventory().addItem(new ItemStack(Material.GOLD_HELMET));
					removeCoins(p, 2);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.goldArmor(1)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
				{
					p.getInventory().addItem(new ItemStack(Material.GOLD_CHESTPLATE));
					removeCoins(p, 2);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.goldArmor(2)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
				{
					p.getInventory().addItem(new ItemStack(Material.GOLD_LEGGINGS));
					removeCoins(p, 2);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.goldArmor(3)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
				{
					p.getInventory().addItem(new ItemStack(Material.GOLD_BOOTS));
					removeCoins(p, 2);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			//Iron Armor
			else if(clicked.equals(ci.ironArmor(0)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(new ItemStack(Material.IRON_HELMET));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.ironArmor(1)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(new ItemStack(Material.IRON_CHESTPLATE));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.ironArmor(2)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(new ItemStack(Material.IRON_LEGGINGS));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			else if(clicked.equals(ci.ironArmor(3)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
				{
					p.getInventory().addItem(new ItemStack(Material.IRON_BOOTS));
					removeCoins(p, 3);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
			
			//Diamond Armor
			else if(clicked.equals(ci.diamondArmor(0)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(new ItemStack(Material.DIAMOND_HELMET));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		    else if(clicked.equals(ci.diamondArmor(1)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(new ItemStack(Material.DIAMOND_CHESTPLATE));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);			
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		    else if(clicked.equals(ci.diamondArmor(2)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(new ItemStack(Material.DIAMOND_LEGGINGS));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		    else if(clicked.equals(ci.diamondArmor(3)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
				{
					p.getInventory().addItem(new ItemStack(Material.DIAMOND_BOOTS));
					removeCoins(p, 5);
					p.updateInventory();
					purchaseConfirm(p);
					
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		}
		else if(e.getClickedInventory().equals(magic))
		{
			ItemStack clicked = e.getInventory().getItem(e.getSlot());
			CustomItems ci = new CustomItems();
			Player p = (Player) e.getWhoClicked();
			if(clicked.equals(ci.blazeRod(0)))
			{
				if(p.getInventory().containsAtLeast(ci.goldCoin(), 0))
				{
					p.getInventory().addItem(ci.blazeRod(0));
					removeCoins(p, 0);
					p.updateInventory();
					purchaseConfirm(p);
				
					e.setCancelled(true);
				}
				else
				{
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't have enough coins to purchase the selected item.");
				}
			}
		}
	}
	
	private void purchaseConfirm(Player p)
	{
		p.sendMessage(ChatColor.GREEN + "Purchase Successful.");
	}
	
	private void removeCoins(Player p, int amount)
	{
		CustomItems ci = new CustomItems();
		for(ItemStack stack : p.getInventory().getContents())
		{
		    if(stack != null)
		    {
		    	if(stack.hasItemMeta())
		    	{
		    		if(stack.getItemMeta().equals(ci.goldCoin().getItemMeta()))
		    		{
		    			if(stack.getAmount() >= amount)
		    			{
		    				if(stack.getAmount() == amount)
		    				{
		    					p.getInventory().remove(stack);
		    					break;
		    				}
		    				else
		    				{
		    					stack.setAmount(stack.getAmount() - amount);
		    					break;
		    				}
		    			}
		    			else
		    			{
		    				int amount2 = stack.getAmount();
		    				amount = amount - amount2;
		    				p.getInventory().remove(stack);
		    			}
		    		}
		    	}
		    }
		}
	}
}
