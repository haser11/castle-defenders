package com.yahoo.kris75tan.CD.Items;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class CustomItems 
{
	/**
	 * Item Lore
	 * Create item lore Strings here
	 */
	
	//ThrowableWall Lore
	private final String WallLore3 = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Creates a 3x3 wall of stone brick where thrown.";
	private final String WallLore3C = ChatColor.GREEN + "Cost: 3 coins";
	
	private final String WallLore5 = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Creates a 5x5 wall of stone brick where thrown.";
	private final String WallLore5C = ChatColor.GREEN + "Cost: 5 coins";
	
	private final String WallLore7 = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Creates a 7x7 wall of stone brick where thrown.";
	private final String WallLore7C = ChatColor.GREEN + "Cost: 7 coins";
	
	private final String WallLoreCustom = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Custom sized wall item, for creative use only.";
	
	//TNT Lore
	private final String tntLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Left click to ignite and throw the TNT.";
	private final String tntLoreC = ChatColor.GREEN + "Cost: 4 coins";
	
	//Gold Coin Lore
	private final String goldCoinLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Gold coins that can be used to purchase items.";
	
	//Food Lore
	private final String meatLore = ChatColor.GREEN + "Cost: 1 coin";
	private final String stewLore = ChatColor.GREEN + "Cost: 2 coins";
	
	//Swords Lore
	private final String woodSwordLore = ChatColor.GREEN + "Cost: 1 coin";
	private final String stoneSwordLore = ChatColor.GREEN + "Cost: 3 coins";
	private final String ironSwordLore = ChatColor.GREEN + "Cost: 5 coins";
	private final String diamondSwordLore = ChatColor.GREEN + "Cost: 7 coins";
	
	//Armor Lore
	private final String leatherArmorLore = ChatColor.GREEN + "Cost: 1 coin";
	private final String goldArmorLore = ChatColor.GREEN + "Cost: 2 coins";
	private final String ironArmorLore = ChatColor.GREEN + "Cost: 3 coins";
	private final String diamondArmorLore = ChatColor.GREEN + "Cost: 5 coins";
	
	//Mage Wand Lore
	private final String golemSpellLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Spawns an Iron Golem on right click";
	private final String golemSpellLoreC = ChatColor.GREEN + "Cost: 5 coins";
	
	private final String golemSnowSpellLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Spawns a Snow Golem on right click";
	private final String golemSnowSpellLoreC = ChatColor.GREEN + "Cost: 2 coins";
	
	private final String fireballSpellLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Shoots a fireball on right click";
	private final String fireballSpellLoreC = ChatColor.GREEN + "Cost: 3 coins";
	
	private final String lightningSpellLore = ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + "Drops lightning on the block your looking at on right click";
	private final String lightningSpellLoreC = ChatColor.GREEN + "Cost: 1 coin";
	
	
	/**
	 * Items
	 * Create custom ItemStacks for global use here
	 */
	
	//ItemStacks Items
	private ItemStack snowballItem = new ItemStack(Material.SNOW_BALL);
	private ItemStack tntItem = new ItemStack(Material.TNT);
	private ItemStack goldCoin = new ItemStack(Material.GOLD_NUGGET);
	private ItemStack blazeRodItem = new ItemStack(Material.BLAZE_ROD);
	
	//ItemStacks Food
	private ItemStack beefItem = new ItemStack(Material.COOKED_BEEF); 
	private ItemStack porkItem = new ItemStack(Material.GRILLED_PORK); 
	private ItemStack chickenItem = new ItemStack(Material.COOKED_CHICKEN); 
	private ItemStack muttonItem = new ItemStack(Material.COOKED_MUTTON);
	private ItemStack stewItem = new ItemStack(Material.RABBIT_STEW); 
	
	//ItemStacks Swords
	private ItemStack woodSword = new ItemStack(Material.WOOD_SWORD);
	private ItemStack stoneSword = new ItemStack(Material.STONE_SWORD);
	private ItemStack ironSword = new ItemStack(Material.IRON_SWORD);
	private ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
	
	//ItemStacks Armor
	private ItemStack leatherHelm = new ItemStack(Material.LEATHER_HELMET);
	private ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE);
	private ItemStack leatherLeg = new ItemStack(Material.LEATHER_LEGGINGS);
	private ItemStack leatherBoot = new ItemStack(Material.LEATHER_BOOTS);
	
	private ItemStack goldHelm = new ItemStack(Material.GOLD_HELMET);
	private ItemStack goldChest = new ItemStack(Material.GOLD_CHESTPLATE);
	private ItemStack goldLeg = new ItemStack(Material.GOLD_LEGGINGS);
	private ItemStack goldBoot = new ItemStack(Material.GOLD_BOOTS);
	
	private ItemStack ironHelm = new ItemStack(Material.IRON_HELMET);
	private ItemStack ironChest = new ItemStack(Material.IRON_CHESTPLATE);
	private ItemStack ironLeg = new ItemStack(Material.IRON_LEGGINGS);
	private ItemStack ironBoot = new ItemStack(Material.IRON_BOOTS);
	
	private ItemStack diamondHelm = new ItemStack(Material.DIAMOND_HELMET);
	private ItemStack diamondChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
	private ItemStack diamondLeg = new ItemStack(Material.DIAMOND_LEGGINGS);
	private ItemStack diamondBoot = new ItemStack(Material.DIAMOND_BOOTS);
	
	//Functions
	public ItemStack snowball(String size)
	{
		if(size.equalsIgnoreCase("3"))
		{
			ItemMeta m = snowballItem.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(WallLore3);
			lore.add(WallLore3C);
			m.setDisplayName(size);
			addMeta(m, snowballItem, lore);
			
			return snowballItem;
		}
		else if(size.equalsIgnoreCase("5"))
		{
			ItemMeta m = snowballItem.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(WallLore5);
			lore.add(WallLore5C);
			m.setDisplayName(size);
			addMeta(m, snowballItem, lore);
			
			return snowballItem;
		}
		else if(size.equalsIgnoreCase("7"))
		{
			ItemMeta m = snowballItem.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(WallLore7);
			lore.add(WallLore7C);
			m.setDisplayName(size);
			addMeta(m, snowballItem, lore);
			
			return snowballItem;
		}
		else
		{
			ItemMeta m = snowballItem.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(WallLoreCustom);
			m.setDisplayName(size);
			addMeta(m, snowballItem, lore);
			
			return snowballItem;
		}
	}
	
	public ItemStack tnt()
	{
		ItemMeta m = tntItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(tntLore);
		lore.add(tntLoreC);
		m.setDisplayName("Dynamite");
		addMeta(m, tntItem, lore);
		
		return tntItem;
	}
	
	public ItemStack goldCoin()
	{
		ItemMeta m = goldCoin.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(goldCoinLore);
		m.setDisplayName("Gold Coin");
		addMeta(m, goldCoin, lore);
		
		return goldCoin;
	}
	
	//Food Items
	public ItemStack beef()
	{
		ItemMeta m = beefItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(meatLore);
		addMeta(m, beefItem, lore);
		
		return beefItem;
	}
	
	public ItemStack pork()
	{
		ItemMeta m = porkItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(meatLore);
		addMeta(m, porkItem, lore);
		
		return porkItem;
	}
	
	public ItemStack chicken()
	{
		ItemMeta m = chickenItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(meatLore);
		addMeta(m, chickenItem, lore);
		
		return chickenItem;
	}
	
	public ItemStack mutton()
	{
		ItemMeta m = muttonItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(meatLore);
		addMeta(m, muttonItem, lore);
		
		return muttonItem;
	}
	
	public ItemStack stew()
	{
		ItemMeta m = stewItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(stewLore);
		addMeta(m, stewItem, lore);
		
		return stewItem;
	}
	
	//Sword Items
	public ItemStack woodSword()
	{
		ItemMeta m = woodSword.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(woodSwordLore);
		addMeta(m, woodSword, lore);
		
		return woodSword;
	}
	
	public ItemStack stoneSword()
	{
		ItemMeta m = stoneSword.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(stoneSwordLore);
		addMeta(m, stoneSword, lore);
		
		return stoneSword;
	}
	
	public ItemStack ironSword()
	{
		ItemMeta m = ironSword.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ironSwordLore);
		addMeta(m, ironSword, lore);
		
		return ironSword;
	}
	
	public ItemStack diamondSword()
	{
		ItemMeta m = diamondSword.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(diamondSwordLore);
		addMeta(m, diamondSword, lore);
		
		return diamondSword;
	}
	
	//Armor Items
	public ItemStack leatherArmor(int type)
	{
		if(type == 0)
		{
			ItemMeta m = leatherHelm.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(leatherArmorLore);
			addMeta(m, leatherHelm, lore);
			
			return leatherHelm;
		}
		else if(type == 1)
		{
			ItemMeta m = leatherChest.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(leatherArmorLore);
			addMeta(m, leatherChest, lore);
			
			return leatherChest;
		}
		else if(type == 2)
		{
			ItemMeta m = leatherLeg.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(leatherArmorLore);
			addMeta(m, leatherLeg, lore);
			
			return leatherLeg;
		}
		else if(type == 3)
		{
			ItemMeta m = leatherBoot.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(leatherArmorLore);
			addMeta(m, leatherBoot, lore);
		
			return leatherBoot;
		}
		else
		{
			return null;
		}
	}
	
	public ItemStack goldArmor(int type)
	{
		if(type == 0)
		{
			ItemMeta m = goldHelm.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(goldArmorLore);
			addMeta(m, goldHelm, lore);
			
			return goldHelm;
		}
		else if(type == 1)
		{
			ItemMeta m = goldChest.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(goldArmorLore);
			addMeta(m, goldChest, lore);
			
			return goldChest;
		}
		else if(type == 2)
		{
			ItemMeta m = goldLeg.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(goldArmorLore);
			addMeta(m, goldLeg, lore);
			
			return goldLeg;
		}
		else if(type == 3)
		{
			ItemMeta m = goldBoot.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(goldArmorLore);
			addMeta(m, goldBoot, lore);
		
			return goldBoot;
		}
		else
		{
			return null;
		}
	}
	
	public ItemStack ironArmor(int type)
	{
		if(type == 0)
		{
			ItemMeta m = ironHelm.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ironArmorLore);
			addMeta(m, ironHelm, lore);
			
			return ironHelm;
		}
		else if(type == 1)
		{
			ItemMeta m = ironChest.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ironArmorLore);
			addMeta(m, ironChest, lore);
			
			return ironChest;
		}
		else if(type == 2)
		{
			ItemMeta m = ironLeg.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ironArmorLore);
			addMeta(m, ironLeg, lore);
			
			return ironLeg;
		}
		else if(type == 3)
		{
			ItemMeta m = ironBoot.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ironArmorLore);
			addMeta(m, ironBoot, lore);
		
			return ironBoot;
		}
		else
		{
			return null;
		}
	}
	
	public ItemStack diamondArmor(int type)
	{
		if(type == 0)
		{
			ItemMeta m = diamondHelm.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(diamondArmorLore);
			addMeta(m, diamondHelm, lore);
			
			return diamondHelm;
		}
		else if(type == 1)
		{
			ItemMeta m = diamondChest.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(diamondArmorLore);
			addMeta(m, diamondChest, lore);
			
			return diamondChest;
		}
		else if(type == 2)
		{
			ItemMeta m = diamondLeg.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(diamondArmorLore);
			addMeta(m, diamondLeg, lore);
			
			return diamondLeg;
		}
		else if(type == 3)
		{
			ItemMeta m = diamondBoot.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(diamondArmorLore);
			addMeta(m, diamondBoot, lore);
		
			return diamondBoot;
		}
		else
		{
			return null;
		}
	}
	
	public ItemStack blazeRod(int spellId)
	{
		ItemMeta m = blazeRodItem.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		m.setDisplayName("Mage Wand");
		
		if(spellId == 0)
		{
			//GOLEM SPELL
			lore.add(golemSpellLore);
			lore.add(golemSpellLoreC);
			addMeta(m, blazeRodItem, lore);
			
			return blazeRodItem;
		}
		else if(spellId == 1)
		{
			lore.add(golemSnowSpellLore);
			lore.add(golemSnowSpellLoreC);
			addMeta(m, blazeRodItem, lore);
			
			return blazeRodItem;
		}
		else if(spellId == 2)
		{
			//FIREBALL SPELL
			lore.add(fireballSpellLore);
			lore.add(fireballSpellLoreC);
			addMeta(m, blazeRodItem, lore);
			
			return blazeRodItem;
		}
		else if(spellId == 3)
		{
			//LIGHTNING SPELL
			lore.add(lightningSpellLore);
			lore.add(lightningSpellLoreC);
			addMeta(m, blazeRodItem, lore);
			
			return blazeRodItem;
		}
		else
		{
			return null;
		}
	}
	
	public void addMeta(ItemMeta m, ItemStack item, ArrayList<String> lore)
	{
		m.setLore(lore);
		item.setItemMeta(m);
	}
	
	

}
