package com.yahoo.kris75tan.CD.Items;

import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.yahoo.kris75tan.CD.Util.FacingDirection;

import net.md_5.bungee.api.ChatColor;

public class MageWand implements Listener
{
	ArrayList<UUID> arrowId = new ArrayList<UUID>();
	Plugin plugin;
	int i = 0;
	int id;
	
	public MageWand(Plugin pluginI)
	{
		plugin = pluginI;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClick(PlayerInteractEvent e)
	{
		Player p = e.getPlayer();
		CustomItems ci = new CustomItems();
		
		if(p.getItemInHand().hasItemMeta())
		{
			if(e.getAction().equals(Action.RIGHT_CLICK_AIR)|| e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			{
				if(p.getItemInHand().equals(ci.blazeRod(0)))
				{
					//IRON GOLEM SPELL
					if(p.getInventory().containsAtLeast(ci.goldCoin(), 5))
					{
						removeCoins(p, 5);
						p.updateInventory();
						p.getWorld().spawnEntity(summonLocation(p), EntityType.IRON_GOLEM);
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 10);
						p.playEffect(p.getLocation(), Effect.EXPLOSION_HUGE, null);
						purchaseConfirm(p);
					}
					else
					{
						purchaseFail(p);
					}
				}
				else if(p.getItemInHand().equals(ci.blazeRod(1)))
				{
					//SNOW GOLEM SPELL
					if(p.getInventory().containsAtLeast(ci.goldCoin(), 2))
					{
						removeCoins(p, 2);
						p.updateInventory();
						p.getWorld().spawnEntity(summonLocation(p), EntityType.SNOWMAN);
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 10);
						p.playEffect(p.getLocation(), Effect.EXPLOSION_HUGE, null);
						purchaseConfirm(p);
					}
					else
					{
						purchaseFail(p);
					}
				}
				else if(p.getItemInHand().equals(ci.blazeRod(2)))
				{
					//FIREBALL SPELL
					if(p.getInventory().containsAtLeast(ci.goldCoin(), 3))
					{
						removeCoins(p, 3);
						p.updateInventory();
						p.launchProjectile(Fireball.class);
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 10);
						p.playEffect(p.getLocation(), Effect.EXPLOSION_HUGE, null);
						purchaseConfirm(p);
					}
					else
					{
						purchaseFail(p);
					}
				}
				else if(p.getItemInHand().equals(ci.blazeRod(3)))
				{
					//LIGHTNING SPELL
					if(p.getInventory().containsAtLeast(ci.goldCoin(), 6))
					{
						removeCoins(p, 6);
						p.updateInventory();
						Location loc = p.getTargetBlock((Set<Material>) null, 500).getLocation();
						p.getWorld().strikeLightning(loc);
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ()));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ()));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ() + 1));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() + 1, loc.getY(), loc.getZ() - 1));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ() + 1));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX() - 1, loc.getY(), loc.getZ() - 1));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ() + 1));
						p.getWorld().strikeLightning(new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ() - 1));
						purchaseConfirm(p);
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 10);
						p.playEffect(p.getLocation(), Effect.EXPLOSION_HUGE, null);
					}
					else
					{
						purchaseFail(p);
					}
				}
			}
			else if(e.getAction().equals(Action.LEFT_CLICK_AIR)|| e.getAction().equals(Action.LEFT_CLICK_BLOCK))
			{
				if(p.getItemInHand().equals(ci.blazeRod(0)))
				{
					p.setItemInHand(ci.blazeRod(1));
					p.sendTitle("", ChatColor.BLUE + "Snow Golem Spell");
					p.updateInventory();
				}
				else if(p.getItemInHand().equals(ci.blazeRod(1)))
				{
					p.setItemInHand(ci.blazeRod(2));
					p.sendTitle("", ChatColor.BLUE + "Fireball Spell");
					p.updateInventory();
				}
				else if(p.getItemInHand().equals(ci.blazeRod(2)))
				{
					p.setItemInHand(ci.blazeRod(3));
					p.sendTitle("", ChatColor.BLUE + "Lightning Spell");
					p.updateInventory();
				}
				else if(p.getItemInHand().equals(ci.blazeRod(3)))
				{
					p.setItemInHand(ci.blazeRod(0));
					p.sendTitle("", ChatColor.BLUE + "Iron Golem Spell");
					p.updateInventory();
				}
			}
		}
	}
	
	@EventHandler
	public void arrowLand(ProjectileHitEvent e)
	{
		if(e.getEntity() instanceof Arrow && arrowId.contains(e.getEntity().getUniqueId()))
		{
			e.getEntity().remove();		
		}
	}
	
	private void purchaseConfirm(Player p)
	{
		p.sendMessage(ChatColor.GREEN + "Purchase Successful.");
	}
	private void purchaseFail(Player p)
	{
		p.sendMessage(ChatColor.RED + "You don't have enough coins to use that spell.");
	}
	
	private Location summonLocation(Player p)
	{
		double x = p.getLocation().getX();
		double y = p.getLocation().getY();
		double z = p.getLocation().getZ();
		
		//NORTH - X
		if(FacingDirection.getCardinalDirection(p).equalsIgnoreCase("north") || 
		FacingDirection.getCardinalDirection(p).equalsIgnoreCase("northeast") ||
		FacingDirection.getCardinalDirection(p).equalsIgnoreCase("northwest"))
		{
			return new Location(p.getWorld(), x - 2, y, z);
		}
		//SOUTH + X
		else if(FacingDirection.getCardinalDirection(p).equalsIgnoreCase("south") ||
		FacingDirection.getCardinalDirection(p).equalsIgnoreCase("southeast") ||
		FacingDirection.getCardinalDirection(p).equalsIgnoreCase("southwest"))
		{
			return new Location(p.getWorld(), x + 2, y, z);
		}
		//EAST - Z
		else if(FacingDirection.getCardinalDirection(p).equalsIgnoreCase("east"))
		{
			return new Location(p.getWorld(), x, y, z - 2);
		}
		//WEST + Z
		else if(FacingDirection.getCardinalDirection(p).equalsIgnoreCase("west"))
		{
			return new Location(p.getWorld(), x, y, z + 2);
		}
		else
		{
			return null;
		}
	}
	
	private void removeCoins(Player p, int amount)
	{
		CustomItems ci = new CustomItems();
		for(ItemStack stack : p.getInventory().getContents())
		{
		    if(stack != null)
		    {
		    	if(stack.hasItemMeta())
		    	{
		    		if(stack.getItemMeta().equals(ci.goldCoin().getItemMeta()))
		    		{
		    			if(stack.getAmount() >= amount)
		    			{
		    				if(stack.getAmount() == amount)
		    				{
		    					p.getInventory().remove(stack);
		    					break;
		    				}
		    				else
		    				{
		    					stack.setAmount(stack.getAmount() - amount);
		    					break;
		    				}
		    			}
		    			else
		    			{
		    				int amount2 = stack.getAmount();
		    				amount = amount - amount2;
		    				p.getInventory().remove(stack);
		    			}
		    		}
		    	}
		    }
		}
	}
}
