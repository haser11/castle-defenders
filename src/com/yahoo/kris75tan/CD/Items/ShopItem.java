package com.yahoo.kris75tan.CD.Items;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopItem implements Listener
{
	@EventHandler
	public void shopItem(PlayerInteractEvent event)
	{
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			if(event.getPlayer().getItemInHand().getType().equals(Material.NETHER_STAR))
			{
				Inventory shop = Bukkit.getServer().createInventory(null, 36, "Shop");
				shop.addItem(new ItemStack(Material.COBBLESTONE));
				shop.addItem(new ItemStack(Material.DIRT));
				shop.addItem(new ItemStack(Material.SNOW));
				shop.addItem(new ItemStack(Material.WOOD));
				shop.addItem(new ItemStack(Material.LOG));
				
				shop.setItem(35, new ItemStack(Material.BED));
				event.getPlayer().openInventory(shop);
				event.getPlayer().sendMessage("SHOP MENU");
			}
		}
	}
}
