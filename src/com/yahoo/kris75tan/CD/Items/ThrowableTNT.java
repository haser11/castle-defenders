package com.yahoo.kris75tan.CD.Items;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ThrowableTNT implements Listener
{
	@EventHandler
	public void throwableTNT(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK))
		{
			CustomItems ci = new CustomItems();
			if(event.getPlayer().getItemInHand().isSimilar(ci.tnt()))
			{
				Player p = event.getPlayer();
				ItemStack stackInHand = p.getItemInHand();
				
				if(stackInHand.getAmount() == 1)
				{
					p.getInventory().remove(stackInHand);
				}
				else
				{
					p.getInventory().getItemInHand().setAmount(stackInHand.getAmount() - 1);
				}
				
				Location tntLoc = new Location(p.getWorld(), p.getLocation().getBlockX(), p.getLocation().getBlockY() + 1, p.getLocation().getBlockZ());
				
				Entity tnt = p.getWorld().spawnEntity(tntLoc, EntityType.PRIMED_TNT);
				tnt.setVelocity(p.getLocation().getDirection());
			}
		}
	}
}
