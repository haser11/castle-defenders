package com.yahoo.kris75tan.CD.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.yahoo.kris75tan.CD.Util.FacingDirection;

public class ThrowableWall implements Listener
{
	
	Player p;
	int num;
	int id = 0;
	
	/**Facing Direction
	0 is SOUTH
	90 is WEST
	180 is NORTH
	270 is EAST
	**/
	@EventHandler
	public void throwItem(ProjectileHitEvent event)
	{
		if(event.getEntity() instanceof Snowball)
		{
			if(id != 0)
			{
			Entity en = event.getEntity();
			Location loc = en.getLocation();
			String dir = FacingDirection.getCardinalDirection(p);
			
			createWall(loc, num, dir);
			id = 0;
			}
		}
	}
	
	@EventHandler
	public void thrower(PlayerInteractEvent event)
	{
		p = event.getPlayer();
		
		if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(p.getItemInHand().getType().equals(Material.SNOW_BALL) && p.getItemInHand().getItemMeta().hasDisplayName())
			{
				String numS = p.getItemInHand().getItemMeta().getDisplayName();
				if(numS != "Snowball")
				{
					num = Integer.parseInt(numS);
					id = 1;
				}
			}
		}
	}
	
	private void createWall(Location botCenterBlock, int size, String direction)
	{
		if(direction.equalsIgnoreCase("west") || direction.equalsIgnoreCase("east"))
		{
			//EAST OR WEST
			//X PLANE
			if(isEvenOrOdd(size) == "EVEN")
			{
			}
			else
			{
				botCenterBlock.getBlock().setType(Material.SMOOTH_BRICK);
				double half = size / 2 - 0.5 + 1;
				int x = (int)botCenterBlock.getX();
				int y = (int)botCenterBlock.getY();
				int z = (int)botCenterBlock.getZ();
				Location startCorner = new Location(botCenterBlock.getWorld(), x + half, y, z);
				Location endCorner = new Location(botCenterBlock.getWorld(), x - half, y + size, z);
				
				for(int xi = (int) startCorner.getX(); xi > endCorner.getX(); xi--)
				{
					for(int yi = (int) startCorner.getY(); yi < endCorner.getY(); yi++)
					{
						Location loc = new Location(botCenterBlock.getWorld(), xi, yi, z);
						loc.getBlock().setType(Material.SMOOTH_BRICK);
					}
				}
			}
		}
		else 
		{
			//NORTH OR SOUTH
			//Z PLANE
			
			if(isEvenOrOdd(size) == "EVEN")
			{
				
			}
			else
			{
				botCenterBlock.getBlock().setType(Material.SMOOTH_BRICK);
				double half = size / 2 - 0.5 + 1;
				int x = (int)botCenterBlock.getX();
				int y = (int)botCenterBlock.getY();
				int z = (int)botCenterBlock.getZ();
				Location startCorner = new Location(botCenterBlock.getWorld(), x, y, z + half);
				Location endCorner = new Location(botCenterBlock.getWorld(), x, y + size, z - half);
				
				for(int zi = (int) startCorner.getZ(); zi > endCorner.getZ(); zi--)
				{
					for(int yi = (int) startCorner.getY(); yi < endCorner.getY(); yi++)
					{
						Location loc = new Location(botCenterBlock.getWorld(), x, yi, zi);
						loc.getBlock().setType(Material.SMOOTH_BRICK);
					}
				}
			
		     }
		}
	}
	
	public String isEvenOrOdd(int num)
	{
		if ( (num & 1) == 0 )
		{
			return "EVEN";
		}
		else
		{
			return "ODD";
		}
	}
}

