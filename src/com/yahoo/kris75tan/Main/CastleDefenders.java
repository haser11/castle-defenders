package com.yahoo.kris75tan.Main;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.yahoo.kris75tan.CD.Commands.CoinCommand;
import com.yahoo.kris75tan.CD.Commands.RenameCommand;
import com.yahoo.kris75tan.CD.Commands.SnowballCommand;
import com.yahoo.kris75tan.CD.Commands.TntCommand;
import com.yahoo.kris75tan.CD.Entities.VillagerShop;
import com.yahoo.kris75tan.CD.Items.MageWand;
import com.yahoo.kris75tan.CD.Items.ShopItem;
import com.yahoo.kris75tan.CD.Items.ThrowableTNT;
import com.yahoo.kris75tan.CD.Items.ThrowableWall;

public class CastleDefenders extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		Bukkit.getLogger().log(Level.INFO, "Castle Defenders is starting up...");
		Bukkit.getPluginManager().registerEvents(new ShopItem(), this);
		Bukkit.getPluginManager().registerEvents(new ThrowableTNT(), this);
		Bukkit.getPluginManager().registerEvents(new ThrowableWall(), this);
		Bukkit.getPluginManager().registerEvents(new VillagerShop(), this);
		Bukkit.getPluginManager().registerEvents(new MageWand(this), this);
		
		getCommand("wall").setExecutor(new SnowballCommand());
		getCommand("coin").setExecutor(new CoinCommand());
		getCommand("tnt").setExecutor(new TntCommand());
		getCommand("rename").setExecutor(new RenameCommand());
	}
	
	@Override
	public void onDisable()
	{
		Bukkit.getLogger().log(Level.INFO, "Castle Defenders is shutting down....");
		Bukkit.getScheduler().cancelAllTasks();
	}
}
